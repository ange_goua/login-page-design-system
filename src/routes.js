import Vue from 'vue'
import VueRouter from 'vue-router'
import SignIn from './components/04-pages/SignIn'
import SignUp from './components/04-pages/SignUp'
import forgotPassword from './components/04-pages/ForgotPassword.vue'

Vue.use(VueRouter)

const router = new VueRouter({
    mode: 'history',
    routes: [{
            path: '/',
            component: SignIn
        },
        {
            path: '/signup',
            component: SignUp
        },
        {
            path: '/forgot-password',
            component: forgotPassword
        }
    ]
})

export default router
